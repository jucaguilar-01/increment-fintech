package com.tenpo.increase.service;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;

public interface UserService {

    UserDTO create(UserDTO request, String url);

    UserDTO login(CredentialDTO credentialDTO, String url);
}
