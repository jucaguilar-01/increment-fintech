package com.tenpo.increase.service.impl;

import com.tenpo.increase.dto.MetricDTO;
import com.tenpo.increase.dto.PageResponseTO;
import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.repository.MetricRepository;
import com.tenpo.increase.service.MetricService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class MetricServiceImpl implements MetricService {

    private final MetricRepository metricRepository;

    @Override
    public PageResponseTO<MetricDTO> getHistory(int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<MetricEntity> response = metricRepository.findAll(paging);
        return buildPageResponse(response);
    }


    private PageResponseTO<MetricDTO> buildPageResponse(Page<MetricEntity> metricEntityPage){
        PageResponseTO<MetricDTO> pageResponse = new PageResponseTO<>();
        List<MetricDTO> dto = metricEntityPage.get().map(this::toDTO).collect(Collectors.toList());
        pageResponse.setResults(dto);
        pageResponse.setTotalPages(metricEntityPage.getTotalPages());
        pageResponse.setTotalElements(metricEntityPage.getTotalElements());

        return pageResponse;
    }

    private MetricDTO toDTO(MetricEntity metricEntity){
        return MetricDTO.builder().id(metricEntity.getId()).endPoint(metricEntity.getEndpoint())
                .response(metricEntity.getResponse()).build();
    }
}
