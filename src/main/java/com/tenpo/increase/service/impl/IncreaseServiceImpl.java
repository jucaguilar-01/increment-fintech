package com.tenpo.increase.service.impl;

import com.tenpo.increase.dto.CoupleNumberDTO;
import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.provider.IncreaseClient;
import com.tenpo.increase.provider.dto.PercentageDTO;
import com.tenpo.increase.service.IncreaseService;
import com.tenpo.increase.utils.TaskMetric;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IncreaseServiceImpl implements IncreaseService {

    private final IncreaseClient increaseClient;

    private final TaskMetric taskMetric;

    @Override
    public IncrementDTO increment(CoupleNumberDTO coupleNumberDTO, String url) {
        Double percentage = invokingClient();
        IncrementDTO dto = calculate(percentage, coupleNumberDTO);
        taskMetric.increaseResource(url,dto);
        return dto;
    }

    private IncrementDTO calculate(Double percentage, CoupleNumberDTO coupleNumberDTO){
        Double initSum =  (coupleNumberDTO.getNumberOne() + coupleNumberDTO.getNumberTwo());
        Double increment = (percentage / 100) * initSum;
        Double total = increment + initSum;
        return IncrementDTO.builder().increment(total).build();
    }

    @Cacheable(value = "increment_cache")
    private Double invokingClient(){
        PercentageDTO response =  this.increaseClient.getPercentage();
        return response.getPercentage();
    }
}
