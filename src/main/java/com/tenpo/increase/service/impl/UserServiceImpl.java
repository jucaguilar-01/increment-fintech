package com.tenpo.increase.service.impl;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.exception.ResourceNotFoundException;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.CredentialEntity;
import com.tenpo.increase.entity.UserEntity;
import com.tenpo.increase.mapper.UserMapper;
import com.tenpo.increase.repository.CredentialRepository;
import com.tenpo.increase.repository.UserRepository;
import com.tenpo.increase.service.UserService;
import com.tenpo.increase.utils.TaskMetric;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Base64;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final CredentialRepository credentialRepository;

    private final UserMapper userMapper;

    private final TaskMetric taskMetric;

    @Transactional
    @Override
    public UserDTO create(UserDTO request, String  url) {
        UserEntity entity = this.userRepository.save(this.userMapper.toEntity(request));
        UserDTO dto = this.userMapper.toDTO(entity);
        taskMetric.userResource(url, dto);
        return dto;
    }

    @Override
    public UserDTO login(CredentialDTO credentialDTO, String url) {
        String password64 = Base64.getEncoder().encodeToString(credentialDTO.getPassword().getBytes());
        CredentialEntity entity = this.credentialRepository.findByUserNameAndPassword(credentialDTO.getUserName(), password64)
                        .orElseThrow(() -> new ResourceNotFoundException("user Credential Not Found"));
        UserDTO dto = this.userMapper.toDTO(entity.getUser());
        taskMetric.userResource(url, dto);
        return dto;
    }
}
