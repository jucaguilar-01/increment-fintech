package com.tenpo.increase.service;

import com.tenpo.increase.dto.MetricDTO;
import com.tenpo.increase.dto.PageResponseTO;
import com.tenpo.increase.entity.MetricEntity;
import org.springframework.data.domain.Page;

public interface MetricService {

    PageResponseTO<MetricDTO> getHistory(int page, int size);
}
