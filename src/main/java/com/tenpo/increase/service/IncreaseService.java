package com.tenpo.increase.service;

import com.tenpo.increase.dto.CoupleNumberDTO;
import com.tenpo.increase.dto.IncrementDTO;

public interface IncreaseService {

    IncrementDTO increment(CoupleNumberDTO coupleNumberDTO, String url);
}
