package com.tenpo.increase.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.exception.ParserException;
import com.tenpo.increase.repository.MetricRepository;

public class ProcessTask<T> {

    private MetricRepository repository;

    public ProcessTask(MetricRepository repository){
        this.repository = repository;
    }

    public void execute(String endpoint , T dto){
        EvenLoopSingleton evenLoopSingleton = EvenLoopSingleton.getInstance();
        ObjectMapper mapper = new ObjectMapper();
        try{
            String jsonInString = mapper.writeValueAsString(dto);
            evenLoopSingleton.execute(new ThreadMetric(repository, buildMetricEntity(endpoint, jsonInString)));
        } catch (JsonProcessingException ex) {
           throw new ParserException(ex.getMessage());
        }
    }

    private MetricEntity buildMetricEntity(String endpoint, String jsonInString){
        return MetricEntity.builder()
                .endpoint(endpoint).response(jsonInString)
                .build();
    }
}
