package com.tenpo.increase.utils;


import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.repository.MetricRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThreadMetric implements Runnable {

    private MetricRepository metricRepository;

    private MetricEntity metricEntity;

    @Override
    public void run() {
        metricRepository.save(this.metricEntity);
    }
}
