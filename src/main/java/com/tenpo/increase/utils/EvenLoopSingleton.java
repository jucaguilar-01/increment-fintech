package com.tenpo.increase.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class EvenLoopSingleton {

    private static EvenLoopSingleton instance;

    @Value("${app.properties.thread-Pool:3}")
    private int threadPool;

    private EvenLoopSingleton(){}

    public static synchronized EvenLoopSingleton getInstance(){
        if(instance == null){
            instance = new EvenLoopSingleton();
        }
        return instance;
    }

    public void execute(Runnable thread){
        ExecutorService executorService = Executors.newFixedThreadPool(6);
        executorService.execute(thread);
    }
}
