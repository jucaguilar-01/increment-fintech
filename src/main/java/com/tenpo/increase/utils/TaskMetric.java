package com.tenpo.increase.utils;

import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.repository.MetricRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TaskMetric {

    private final MetricRepository metricRepository;

    public void userResource(String endpoint, UserDTO dto){
        ProcessTask<UserDTO> processTask = new ProcessTask<>(metricRepository);
        processTask.execute(endpoint, dto);
    }

    public void increaseResource(String endpoint, IncrementDTO dto){
        ProcessTask<IncrementDTO> processTask = new ProcessTask<>(metricRepository);
        processTask.execute(endpoint, dto);
    }
}
