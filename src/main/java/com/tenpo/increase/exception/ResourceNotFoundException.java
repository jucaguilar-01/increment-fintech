package com.tenpo.increase.exception;

import lombok.Data;

@Data
public class ResourceNotFoundException extends RuntimeException {

    private static final String MESSAGE = "Resource Not Found";

    private String detail;

    public ResourceNotFoundException(String detail) {
        super(MESSAGE);
        this.detail = detail;
    }
}