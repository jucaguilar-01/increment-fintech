package com.tenpo.increase.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(value = BadRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ErrorResponseDTO handleBadRequestException(BadRequestException ex) {
		return ErrorResponseDTO.builder().httpMessage(ex.getMessage())
				.httpDescription(ex.getDetail()).build();
	}

	@ExceptionHandler(value = ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	protected ErrorResponseDTO handleNotFoundException(ResourceNotFoundException ex) {
		return ErrorResponseDTO.builder().httpMessage(ex.getMessage())
				.httpDescription(ex.getDetail()).build();
	}

	@ExceptionHandler(value = FeignClientException.class)
	@ResponseStatus(HttpStatus.BAD_GATEWAY)
	protected ErrorResponseDTO handleBadGatewayException(FeignClientException ex) {
		return ErrorResponseDTO.builder().httpMessage(ex.getMessage())
				.httpDescription(ex.getDetail()).build();
	}

	@ExceptionHandler(value = ParserException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected ErrorResponseDTO handleParserException(ParserException ex) {
		return ErrorResponseDTO.builder().httpMessage(ex.getMessage())
				.httpDescription(ex.getDetail()).build();
	}

	@ExceptionHandler(value = RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected ErrorResponseDTO handleRuntimeException(RuntimeException ex) {
		return ErrorResponseDTO.builder().httpMessage("Application Error processing the request")
				.httpDescription(ex.getMessage()).build();
	}

	@ExceptionHandler(value = Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected ErrorResponseDTO handleGenericException(Exception ex) {
		return ErrorResponseDTO.builder().httpMessage("unexpected error in server")
				.httpDescription(ex.getMessage()).build();
	}
}
