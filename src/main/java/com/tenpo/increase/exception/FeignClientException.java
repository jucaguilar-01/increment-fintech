package com.tenpo.increase.exception;

import lombok.Data;

@Data
public class FeignClientException extends RuntimeException {

    private static final String MESSAGE = "Feign Api Rest connection failed: ";

    private String detail;

    public FeignClientException(String detail){
        super(MESSAGE);
        this.detail = detail;
    }
}
