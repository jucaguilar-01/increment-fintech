package com.tenpo.increase.exception;

import lombok.Data;

@Data
public class BadRequestException extends RuntimeException {

    private static final String MESSAGE = "Bad Request Exception: ";

    private String detail;

    public BadRequestException(String detail){
     super(MESSAGE);
     this.detail = detail;
    }
}
