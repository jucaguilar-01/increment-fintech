package com.tenpo.increase.exception;

import lombok.Data;

@Data
public class ParserException extends RuntimeException {

    private static final String MESSAGE = "error Parser while the request was been processing: ";

    private String detail;

    public ParserException(String detail){
        super(MESSAGE);
        this.detail = detail;
    }
}
