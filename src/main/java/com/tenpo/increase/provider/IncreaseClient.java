package com.tenpo.increase.provider;

import com.tenpo.increase.provider.config.ErrorDecoderProvider;
import com.tenpo.increase.provider.dto.PercentageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "increaseClient", url = "https://run.mocky.io/v3", configuration = IncreaseClient.Config.class)
public interface IncreaseClient {

    @GetMapping(value = "/0af4e842-1e82-4b44-ae49-b75996be87f8")
    PercentageDTO getPercentage();

    class Config {

        @Bean
        public ErrorDecoderProvider edenErrorDecoder() {
            return new ErrorDecoderProvider();
        }
    }
}
