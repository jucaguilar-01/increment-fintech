package com.tenpo.increase.provider.config;

import lombok.Data;

@Data
public class ErrorProviderResponse {

    private String message;
}
