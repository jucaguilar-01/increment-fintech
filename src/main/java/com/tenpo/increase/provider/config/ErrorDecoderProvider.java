package com.tenpo.increase.provider.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.increase.exception.FeignClientException;
import feign.FeignException;
import feign.Response;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.io.IOException;

import static java.util.Objects.nonNull;

@Slf4j
public class ErrorDecoderProvider implements ErrorDecoder {

    private static final String NO_MESSAGE = "No error description from destination";
    private static final String FORMAT_MESSAGE = "Can not parse payload. Verify the format response set by provider";

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() >= 500) {
            return buildRetry(methodKey, response);
        }
        HttpStatus httpStatus = HttpStatus.valueOf(response.status());
        String detailErrorResponse = this.extractMessage(response.body());
        log.warn("HTTP status {} for {}. Error message {}", httpStatus, response.request(), detailErrorResponse);
        throw new FeignClientException(detailErrorResponse);
    }

    private RetryableException buildRetry(String methodKey, Response response){
        FeignException exception = feign.FeignException.errorStatus(methodKey, response);
            return new RetryableException(
                    response.status(),
                    exception.getMessage(),
                    response.request().httpMethod(),
                    exception,
                    null,
                    response.request()
                    );
    }

    private String extractMessage(final Response.Body body) {

        ObjectMapper objectMapper = new ObjectMapper();

        if (nonNull(body)) {
            try {
                ErrorProviderResponse response = objectMapper.readValue(body.asInputStream(), ErrorProviderResponse.class);
                return response.getMessage();
            } catch (IOException e) {
                log.error(FORMAT_MESSAGE, e);
                throw new RuntimeException(FORMAT_MESSAGE);
            }

        } else {
            return NO_MESSAGE;
        }
    }
}
