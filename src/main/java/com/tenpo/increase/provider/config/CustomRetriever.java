package com.tenpo.increase.provider.config;

import com.tenpo.increase.exception.FeignClientException;
import feign.RetryableException;
import feign.Retryer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@NoArgsConstructor
public class CustomRetriever implements Retryer {

    private int maxAttempt;

    private long interval;

    private int attempt = 1;

    public CustomRetriever(int maxAttempt, long interval){
     this.maxAttempt = maxAttempt;
     this.interval = interval;
    }

    @Override
    public void continueOrPropagate(RetryableException e) {
        log.info("Feign retry attempt {} because {} with interval {}", attempt, e.getMessage(), interval);
        if (attempt++ == maxAttempt) {
            log.info("the max number of Attempts was reached, the error must be propagated");
            throw new FeignClientException("exhausted feign attempts: " + e.getMessage());
        }
        try {
            Thread.sleep(interval);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public Retryer clone() {
        return new CustomRetriever(3,2000);
    }
}
