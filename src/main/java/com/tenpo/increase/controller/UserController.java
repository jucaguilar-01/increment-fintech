package com.tenpo.increase.controller;

import com.tenpo.increase.aspect.LoggerMetric;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("api/v1/tenpo")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @LoggerMetric
    @PostMapping(value = "/user", produces =  MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UserDTO> signUp(@Valid @RequestBody UserDTO request, HttpServletRequest http) {
        UserDTO response = userService.create(request,http.getRequestURI());
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @LoggerMetric
    @GetMapping(value = "/user", produces =  MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<UserDTO> login(@RequestParam(name = "userName") String userName,
                                  @RequestParam(name = "password") String password,
                                  HttpServletRequest http) {
        log.info("Entering method login user ClientController- " + LocalDateTime.now());
        UserDTO response = userService.login(CredentialDTO.builder().userName(userName)
                .password(password).build(), http.getRequestURI());
        log.info("leaving method login user ClientController- " + LocalDateTime.now());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
