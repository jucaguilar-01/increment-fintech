package com.tenpo.increase.controller;

import com.tenpo.increase.aspect.LoggerMetric;
import com.tenpo.increase.dto.CoupleNumberDTO;
import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.service.IncreaseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("api/v1/tenpo")
@RequiredArgsConstructor
public class IncreaseController {

    private final IncreaseService increaseService;

    @LoggerMetric
    @GetMapping(value = "/increase", produces =  MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<IncrementDTO> recalculate(@RequestParam(name = "numberOne") Double numberOne,
                                             @RequestParam(name = "numberTwo") Double numberTwo,
                                             HttpServletRequest http) {
        IncrementDTO response = this.increaseService.increment(CoupleNumberDTO.builder().numberOne(numberOne)
                .numberTwo(numberTwo).build(), http.getRequestURI());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
