package com.tenpo.increase.controller;

import com.tenpo.increase.aspect.LoggerMetric;
import com.tenpo.increase.dto.MetricDTO;
import com.tenpo.increase.dto.PageResponseTO;
import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.service.MetricService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("api/v1/tenpo/metric")
@RequiredArgsConstructor
public class MetricController {

    private final MetricService metricService;

    @LoggerMetric
    @GetMapping(value = "/history", produces =  MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<PageResponseTO<MetricDTO>> login(@RequestParam(defaultValue = "0") int page,
                                                    @RequestParam(defaultValue = "50") int size) {
        log.info("Entering method login user ClientController- " + LocalDateTime.now());
        PageResponseTO<MetricDTO> response = this.metricService.getHistory(page,size);
        log.info("leaving method login user ClientController- " + LocalDateTime.now());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
