package com.tenpo.increase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageResponseTO<T> {

    private List<T> results;
    private Integer totalPages;
    private Long totalElements;
}
