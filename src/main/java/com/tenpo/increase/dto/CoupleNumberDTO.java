package com.tenpo.increase.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoupleNumberDTO {

    private Double numberOne;

    private Double numberTwo;
}
