package com.tenpo.increase.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CredentialDTO implements Serializable {

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("password")
    private String password;
}
