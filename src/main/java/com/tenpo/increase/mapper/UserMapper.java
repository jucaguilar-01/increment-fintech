package com.tenpo.increase.mapper;

import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.UserEntity;

public interface UserMapper {

    UserEntity toEntity(UserDTO dto);
    UserDTO toDTO(UserEntity entity);

}
