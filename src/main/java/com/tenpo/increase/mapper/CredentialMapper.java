package com.tenpo.increase.mapper;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.entity.CredentialEntity;

public interface CredentialMapper {

    CredentialEntity toEntity(CredentialDTO dto);
    CredentialDTO toDTO(CredentialEntity entity);
}
