package com.tenpo.increase.mapper.impl;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.CredentialEntity;
import com.tenpo.increase.entity.UserEntity;
import com.tenpo.increase.mapper.CredentialMapper;
import com.tenpo.increase.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMapperImpl implements UserMapper {

    private final CredentialMapper credentialMapper;

    @Override
    public UserEntity toEntity(UserDTO dto) {

        if (dto == null) {
            return null;
        }

        UserEntity entity = new UserEntity();
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());

        CredentialEntity credentialEntity = this.credentialMapper.toEntity(dto.getCredential());

        entity.setCredential(credentialEntity);

        return entity;
    }

    @Override
    public UserDTO toDTO(UserEntity entity) {

        if (entity == null) {
            return null;
        }

        UserDTO dto = new UserDTO();
        dto.setUserId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());

        CredentialDTO credentialDTO = this.credentialMapper.toDTO(entity.getCredential());

        dto.setCredential(credentialDTO);

        return dto;
    }
}
