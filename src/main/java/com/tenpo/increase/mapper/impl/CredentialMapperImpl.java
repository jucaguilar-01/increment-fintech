package com.tenpo.increase.mapper.impl;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.entity.CredentialEntity;
import com.tenpo.increase.mapper.CredentialMapper;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class CredentialMapperImpl implements CredentialMapper {

    public CredentialEntity toEntity(CredentialDTO dto) {

        if (dto == null) {
            return null;
        }

        CredentialEntity entity = new CredentialEntity();
        entity.setPassword(encoding64(dto.getPassword()));
        entity.setUserName(dto.getUserName());

        return entity;
    }

    @Override
    public CredentialDTO toDTO(CredentialEntity entity) {

        if (entity == null) {
            return null;
        }

        CredentialDTO dto = new CredentialDTO();
        dto.setUserName(entity.getUserName());
        dto.setPassword(decoding64(entity.getPassword()));

        return dto;
    }

    private String encoding64(String encoding) {
        return Base64.getEncoder().encodeToString(encoding.getBytes());
    }

    private String decoding64(String decoding) {
        byte[] decodedBytes = Base64.getDecoder().decode(decoding);
        return new String(decodedBytes);
    }
}
