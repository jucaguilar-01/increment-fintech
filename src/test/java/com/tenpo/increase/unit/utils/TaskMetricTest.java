package com.tenpo.increase.unit.utils;


import com.tenpo.increase.FactoryObjetMock;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.repository.MetricRepository;
import com.tenpo.increase.utils.TaskMetric;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class TaskMetricTest {

    private TaskMetric taskMetric;

    @Mock
    private MetricRepository metricRepository;

    private UserDTO request;

    @BeforeEach
    void init() {
        lenient().when(metricRepository.save(any())).thenReturn(FactoryObjetMock.MetricEntity());
        this.taskMetric = new TaskMetric(this.metricRepository);

        this.request = UserDTO.builder().name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar").password("123").build()).build();
    }


    @DisplayName("case: execute metric to resource User finish OK")
    @Test
    public void testOne() {
        taskMetric.userResource("/api/v1/user", request);

        Assertions.assertTrue(true, "the test happened with problems");
    }

    @DisplayName("case: execute metric to calculate increment finish OK")
    @Test
    public void testTwo() {
        IncrementDTO request = IncrementDTO.builder().increment(20D).build();

        taskMetric.increaseResource("/api/v1/user", request);

        Assertions.assertTrue(true, "the test happened with problems");
    }
}
