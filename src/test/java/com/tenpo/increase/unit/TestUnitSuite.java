package com.tenpo.increase.unit;

import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@SelectPackages({"com.tenpo.increase.unit.mapper","com.tenpo.increase.unit.service","com.tenpo.increase.utils"})
@Suite
@SuiteDisplayName("suit to unit test")
public class TestUnitSuite {
}
