package com.tenpo.increase.unit.service;

import com.tenpo.increase.dto.CoupleNumberDTO;
import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.provider.IncreaseClient;
import com.tenpo.increase.provider.dto.PercentageDTO;
import com.tenpo.increase.service.IncreaseService;
import com.tenpo.increase.service.impl.IncreaseServiceImpl;
import com.tenpo.increase.utils.TaskMetric;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class IncreaseServiceTest {

    private IncreaseService increaseService;

    @Mock
    private  IncreaseClient feignClient;

    @Mock
    private TaskMetric taskMetric;

    private CoupleNumberDTO request;

    @BeforeEach
    void init() {
        lenient().doNothing().when(taskMetric).increaseResource(any(),any());
        this.increaseService = new IncreaseServiceImpl(this.feignClient,this.taskMetric);
        this.request = CoupleNumberDTO.builder()
                .numberOne(10D).numberTwo(10D).build();
    }

    @DisplayName("case: calculate increment finish OK")
    @Test
    public void testOne() {
        when(feignClient.getPercentage()).thenReturn(PercentageDTO.builder().percentage(20D).build());

        IncrementDTO response = increaseService.increment(request, "/api/v1/increase");

        Assertions.assertEquals(24, response.getIncrement().intValue());
    }

    @DisplayName("case: calculate increment but error feign client, it should Throw a exception")
    @Test
    public void testTwo() {
        when(feignClient.getPercentage()).thenThrow(RuntimeException.class);

        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            increaseService.increment(request, "/api/v1/increase");
        }, "Exception was expected");

        Assertions.assertNotNull(exception);
    }


}
