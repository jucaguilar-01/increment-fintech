package com.tenpo.increase.unit.service;

import com.tenpo.increase.FactoryObjetMock;
import com.tenpo.increase.dto.CoupleNumberDTO;
import com.tenpo.increase.dto.IncrementDTO;
import com.tenpo.increase.dto.MetricDTO;
import com.tenpo.increase.dto.PageResponseTO;
import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.repository.MetricRepository;
import com.tenpo.increase.service.MetricService;
import com.tenpo.increase.service.impl.MetricServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MetricServiceTest {

    @Mock
    private MetricRepository metricRepository;

    private MetricService metricService;

    @BeforeEach
    void init() {
        this.metricService = new MetricServiceImpl(this.metricRepository);
    }

    @DisplayName("case: get metric finish OK")
    @Test
    public void testOne() {

        Page<MetricEntity> pageMock = new PageImpl(FactoryObjetMock.buildMetricEntity());

        when(metricRepository.findAll(any(Pageable.class))).thenReturn(pageMock);

        PageResponseTO<MetricDTO> response = metricService.getHistory(0,10);

        Assertions.assertEquals(2L, response.getTotalElements());
    }
}
