package com.tenpo.increase.unit.service;

import com.tenpo.increase.FactoryObjetMock;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.UserEntity;
import com.tenpo.increase.exception.ResourceNotFoundException;
import com.tenpo.increase.mapper.UserMapper;
import com.tenpo.increase.repository.CredentialRepository;
import com.tenpo.increase.repository.UserRepository;
import com.tenpo.increase.service.UserService;
import com.tenpo.increase.service.impl.UserServiceImpl;
import com.tenpo.increase.utils.TaskMetric;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CredentialRepository credentialRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private TaskMetric taskMetric;

    private UserDTO request;

    @BeforeEach
    void init() {
        lenient().doNothing().when(taskMetric).userResource(any(),any());
        this.userService = new UserServiceImpl(this.userRepository, this.credentialRepository
        ,this.userMapper, this.taskMetric);

        this.request = UserDTO.builder().name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar")
                        .password("123").build()).build();
    }

    @DisplayName("case: create a user and finish OK")
    @Test
    public void testOne() {
            when(userMapper.toEntity(any())).thenReturn(FactoryObjetMock.userMapperEntity());
            when(userRepository.save(any(UserEntity.class))).thenReturn(FactoryObjetMock.buildUserEntity());
            when(userMapper.toDTO(any())).thenReturn(FactoryObjetMock.userMapperDTO());

            UserDTO response = userService.create(request, "/api/vi/user");

            Assertions.assertTrue(response.getUserId() == 1L && response.getCredential() != null);
    }

    @DisplayName("case: create a user but error connecting a db, it should Throw a exception")
    @Test
    public void testTwo() {
        when(userMapper.toEntity(any())).thenReturn(FactoryObjetMock.userMapperEntity());
        when(userRepository.save(any(UserEntity.class))).thenThrow(RuntimeException.class);

        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            userService.create(request, "/api/vi/user");
        }, "RuntimeException was expected");

        Assertions.assertNotNull(exception);
    }

    @DisplayName("case: login a user and finish OK")
    @Test
    public void testTree() {

        when(credentialRepository.findByUserNameAndPassword(any(),any())).thenReturn(FactoryObjetMock.buildCredentialEntity());
        when(userMapper.toDTO(any())).thenReturn(FactoryObjetMock.userMapperDTO());

        UserDTO response = userService.login(CredentialDTO.builder()
                .userName("jucaguilar").password("123").build(), "/api/vi/user");

        Assertions.assertTrue(response.getUserId() == 1L
                && response.getCredential().getUserName().equals("jucaguilar")
                && response.getCredential().getPassword().equals("123"));
    }

    @DisplayName("case: login a user but Not found credentials, it should Throw a ResourceNotFoundException")
    @Test
    public void testFour() {
        when(credentialRepository.findByUserNameAndPassword(any(),any())).thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            userService.login(CredentialDTO.builder()
                    .userName("jucaguilar").password("123").build(), "/api/vi/user");
        }, "ResourceNotFoundException was expected");

        Assertions.assertEquals("user Credential Not Found", exception.getDetail());
    }
}
