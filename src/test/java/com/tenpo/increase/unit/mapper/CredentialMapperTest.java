package com.tenpo.increase.unit.mapper;


import com.tenpo.increase.FactoryObjetMock;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.entity.CredentialEntity;
import com.tenpo.increase.mapper.CredentialMapper;
import com.tenpo.increase.mapper.impl.CredentialMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Base64;

public class CredentialMapperTest {

    private CredentialMapper credentialMapper;

    private CredentialDTO request;

    @BeforeEach
    void init(){

        this.credentialMapper = new CredentialMapperImpl();
        this.request = CredentialDTO.builder().userName("jucaguilar")
                .password("123").build();
    }

    @DisplayName("case: mapper credential DTO to entity finish OK")
    @Test
    public void testOne(){

        CredentialEntity response = credentialMapper.toEntity(request);

        Assertions.assertTrue(response.getUserName().equals("jucaguilar")
                && response.getPassword().equals(encoding64Test("123")));
    }

    @DisplayName("case: mapper credential entity to dto finish OK")
    @Test
    public void testTwo(){

        CredentialDTO response = credentialMapper.toDTO(FactoryObjetMock.CredentialMapperEntity());

        Assertions.assertTrue(response.getUserName().equals("jucaguilar")
                && response.getPassword().equals("123"));
    }

    private String encoding64Test(String encoding) {
        return Base64.getEncoder().encodeToString(encoding.getBytes());
    }
}
