package com.tenpo.increase.unit.mapper;


import com.tenpo.increase.FactoryObjetMock;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.UserEntity;
import com.tenpo.increase.mapper.CredentialMapper;
import com.tenpo.increase.mapper.UserMapper;
import com.tenpo.increase.mapper.impl.UserMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserMapperTest {

    private UserMapper userMapper;

    @Mock
    private CredentialMapper credentialMapper;

    private UserDTO request;

    @BeforeEach
    void init(){

        this.userMapper = new UserMapperImpl(this.credentialMapper);
        this.request = UserDTO.builder().name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar")
                        .password("123").build()).build();
    }

    @DisplayName("case: mapper user DTO to entity finish OK")
    @Test
    public void testOne(){

        when(credentialMapper.toEntity(any())).thenReturn(FactoryObjetMock.CredentialMapperEntity());

        UserEntity response = userMapper.toEntity(request);

        Assertions.assertTrue(response.getName().equals("juan")
                && response.getEmail().equals("aguilar@gmail.com") && response.getCredential() != null);
    }

    @DisplayName("case: mapper user entity to dto finish OK")
    @Test
    public void testTwo(){

        when(credentialMapper.toDTO(any())).thenReturn(CredentialDTO.builder()
                .userName("juan").password("123").build());

        UserDTO response = userMapper.toDTO(FactoryObjetMock.userMapperEntity());

        Assertions.assertTrue(response.getName().equals("juan")
                && response.getEmail().equals("aguilar@gmail.com") && response.getCredential() != null);
    }
}
