package com.tenpo.increase.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.increase.MsIncreaseTenpoApplication;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MsIncreaseTenpoApplication.class })
@TestPropertySource(value = {"classpath:application-test.yml"})
@AutoConfigureMockMvc
@WebAppConfiguration
public class IncreaseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Disabled("Disabled: only should get status 5XX if Api rest external is off-line")
    @Test
    public void getIncrementShouldThrowStatus5XX() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/tenpo/increase")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("numberOne", "10")
                        .param("numberTwo", "10"))
                .andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.httpMessage").value("Feign Api Rest connection failed: "));
    }

    @DisplayName("enabled: only should get status 200 if Api rest external is online")
    @Test
    public void getIncrementShouldBeStatus200() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/tenpo/increase")
                        .accept(MediaType.APPLICATION_JSON)
                        .param("numberOne", "10")
                        .param("numberTwo", "10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.increment").value("24.0"));
    }
}
