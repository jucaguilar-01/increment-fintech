package com.tenpo.increase.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.increase.MsIncreaseTenpoApplication;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MsIncreaseTenpoApplication.class })
@TestPropertySource(value = {"classpath:application-test.yml"})
@AutoConfigureMockMvc
@WebAppConfiguration
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void userCreated() throws Exception {


        UserDTO request = UserDTO.builder().name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar")
                        .password("123").build()).build();

        mockMvc.perform(
                        MockMvcRequestBuilders.post("/api/v1/tenpo/user")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.name").value("juan"));
    }

    @Test
    public void loginUserOk() throws Exception {


        /*before create user*/
        UserDTO request = UserDTO.builder().name("pedro").email("pedro@gmail.com")
                .credential(CredentialDTO.builder().userName("pedrito")
                        .password("345").build()).build();

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/tenpo/user")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.name").value("pedro"));


        /*then validate credential*/
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/tenpo/user")
                                .param("userName", "pedrito")
                                .param("password", "345")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.name").value("pedro"))
                .andExpect(jsonPath("$.email").value("pedro@gmail.com"));
    }
}
