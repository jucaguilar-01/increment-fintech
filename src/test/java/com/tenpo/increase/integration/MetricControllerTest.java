package com.tenpo.increase.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.increase.MsIncreaseTenpoApplication;
import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { MsIncreaseTenpoApplication.class })
@TestPropertySource(value = {"classpath:application-test.yml"})
@AutoConfigureMockMvc
@WebAppConfiguration
public class MetricControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {

        UserDTO request = UserDTO.builder().name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar")
                        .password("123").build()).build();
        try {

            mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/tenpo/user")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .accept(MediaType.APPLICATION_JSON)
                                    .content(objectMapper.writeValueAsString(request)))
                    .andExpect(status().isCreated());
            System.out.println("data load was ok");
        } catch (Exception x) {
            System.out.println("error init data");
        }
    }

    @Test
    public void getMetricHistoryOK() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/tenpo/metric/history")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(1L));
    }
}
