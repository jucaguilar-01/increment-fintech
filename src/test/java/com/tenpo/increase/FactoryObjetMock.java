package com.tenpo.increase;

import com.tenpo.increase.dto.CredentialDTO;
import com.tenpo.increase.dto.UserDTO;
import com.tenpo.increase.entity.CredentialEntity;
import com.tenpo.increase.entity.MetricEntity;
import com.tenpo.increase.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FactoryObjetMock {

    public static UserEntity buildUserEntity(){

        CredentialEntity credentialEntity = new CredentialEntity();
        credentialEntity.setId(1L);
        credentialEntity.setUserName("jucaguilar");
        credentialEntity.setPassword("MTIz");

        UserEntity entity = new UserEntity();
        entity.setId(1L);
        entity.setName("juan");
        entity.setEmail("aguilar@gmail.com");
        entity.setCredential(credentialEntity);

        return entity;
    }

    public static UserDTO userMapperDTO(){
       return UserDTO.builder().userId(1L).name("juan").email("aguilar@gmail.com")
                .credential(CredentialDTO.builder().userName("jucaguilar")
                        .password("123").build()).build();
    }

    public static UserEntity userMapperEntity(){
        CredentialEntity credentialEntity = new CredentialEntity();
        credentialEntity.setUserName("jucaguilar");
        credentialEntity.setPassword("MTIz");

        UserEntity entity = new UserEntity();
        entity.setName("juan");
        entity.setEmail("aguilar@gmail.com");
        entity.setCredential(credentialEntity);

        return entity;
    }

    public static Optional<CredentialEntity> buildCredentialEntity(){
        CredentialEntity entity = new CredentialEntity();
        entity.setId(1L);
        entity.setUserName("jucaguilar");
        entity.setPassword("MTIz");

        return Optional.of(entity);
    }

    public static CredentialEntity CredentialMapperEntity(){
        CredentialEntity entity = new CredentialEntity();
        entity.setUserName("jucaguilar");
        entity.setPassword("MTIz");

        return entity;
    }

    public static MetricEntity MetricEntity() {
        MetricEntity entity = new MetricEntity();
        entity.setId(1L);
        entity.setEndpoint("/api/v1/increase");
        entity.setResponse("{increment: 23}");

        return entity;
    }

    public static List<MetricEntity> buildMetricEntity() {
        List<MetricEntity> entityList = new ArrayList<>();

        MetricEntity entity1 = new MetricEntity();
        entity1.setId(1L);
        entity1.setEndpoint("/api/v1/increase");
        entity1.setResponse("{increment: 23}");

        entityList.add(entity1);

        MetricEntity entity2 = new MetricEntity();
        entity2.setId(1L);
        entity2.setEndpoint("/api/v1/increase");
        entity2.setResponse("{increment: 34}");

        entityList.add(entity2);

        return entityList;
    }
}
