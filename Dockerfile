FROM adoptopenjdk/openjdk11:latest
RUN mkdir /usr/src/increase
COPY ./target/increase-0.0.1-SNAPSHOT.jar /usr/src/increase
WORKDIR /usr/src/increase
EXPOSE 8061
CMD ["java", "-jar","-Dspring.redis.host=rediscontainer", "/usr/src/increase/increase-0.0.1-SNAPSHOT.jar"]