CREATE TABLE METRIC (
	id BIGSERIAL NOT NULL,
	endpoint VARCHAR(255) NULL,
	response TEXT NULL,
	CONSTRAINT metric_pk PRIMARY KEY (id)
);

CREATE TABLE CREDENTIAL (
	id BIGSERIAL NOT NULL,
	"password" VARCHAR(255) NULL,
	user_name  VARCHAR(255) NULL,
	CONSTRAINT credential_pk PRIMARY KEY (id)
);

CREATE TABLE CUSTOMER (
	id BIGSERIAL NOT NULL,
	 email varchar(255) NULL,
	"name" varchar(255) NULL,
	credential_id int8 NULL,

	CONSTRAINT customer_pk PRIMARY KEY (id)
);

ALTER TABLE customer ADD CONSTRAINT k_customer_credential_id FOREIGN KEY (credential_id) REFERENCES credential (id);