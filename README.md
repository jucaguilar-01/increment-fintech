# challenger by fintech


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_maven_

_java 11_

_docker_

_docker-compose_

_postgres_

puedes verificar esto corriendo el comando

```
mnv -version

java -version

docker version

```

### Instalación 🔧


_Descargar el codigo fuente desde el repositorio de gitlab_

```
git@gitlab.com:jucaguilar-01/increment-fintech.git
```

_ingresar al directorio raiz en donde se ubica la carpeta del proyecto y ejecuta el siguiente comando maven_

_[de esta forma podra correr la aplicacion localmente, recuerde tener una base de datos local posgres instalada]_

```
mvn spring-boot:run
```

una vez levantado el proyecto, el mismo estara escuchando en la URL: http://localhost:8080

## Ejecutando las pruebas ⚙️

_las pruebas se realizan mediante la herramientas Postman_

exporte el archivo (reba.postman_collection.json) ubicacion:

```
../src/resource/static/reba.postman_collection.json
```

### documentacion de aplicavion 🔩

_la documentacion de la Api Rest_

```
http://localhost:8080/swagger-ui/index.html#/
```

### base de datos ⌨️

_el proyecto necesita conectarse a una base local postgres_

```
debera crear una base llamada: db_ubuntu y schema: tenpo
```

## Despliegue 📦

_descargar la imagen desde el repositorio_

```
debera crear una base llamada: db_ubuntu y schema: tenpo
```

_descargar la imagen docker que contiene el proyecto_
```
docker pull jucaguilar/fintech-challenge
```


*  ubiquese en la raiz del proyecto y alli ejecute los comandos

```shell
$ docker-compose build 

$ docker-compose up

```
* cuando quiera termminar la aplicacion

```shell
$ docker-compose down
```

## Construido con 🛠️

_Spring Boot 2.6.4_

## Versionado 📌

codigo fuente es versionado en Gitlab

## Autores ✒️

* **Aguiar juan carlos** - *developer* - [jucaguilar](https://gitlab.com/jucaguilar-01)

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles
